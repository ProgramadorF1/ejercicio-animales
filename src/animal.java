
public class animal {

        // Atributos
    private double Peso;
    private double Altura;
    private String Color;
    private String Nombre;
    
        // Metodo
    public double getPeso() {
        return Peso;
    }

    public void setPeso(double Peso) {
        this.Peso = Peso;
    }

    public double getAltura() {
        return Altura;
    }

    public void setAltura(double Altura) {
        this.Altura = Altura;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    public String mover(){
        return "El animal se esta moviendo";
    }
        //Constructor
    public animal(String Nombre){
        this.Nombre = Nombre;
        this.Color = null;
        this.Peso = 0;
        this.Altura = 0;
    }
}
