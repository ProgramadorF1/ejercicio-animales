
public class principal {
    public static void main(String[] args) {
        animal nm = new animal ("Mateo");
        System.out.println(nm.mover());
        
        
        System.out.println("El nombre de la mascota es "+ nm.getNombre());
        
        nm.setColor("Azul");
        System.out.println("El color de la mascota es "+ nm.getColor());
        
        nm.setAltura(172);
        System.out.println("La altura de la mascota es de "+ nm.getAltura());
        
        nm.setPeso(1200);
        System.out.println("El peso de la mascota es de "+ nm.getPeso());
    }
}
